var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var logger = require('../servicos/logger');
var morgan = require('morgan');



module.exports = function () {
    //receber a invocação do express
    var app = express();

    //Common formato . existes
    app.use(morgan("common", {
        stream: {
            write: function(mensagem){
                logger.info(mensagem);
            }
        }
    }));


    //middleware
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended:true})); //Usando para post de formularios 

    app.use(expressValidator());


    //incluindo pasta ex: controllers dentro do objeto app
    consign()
        .include('controllers')
        .then('persistencia')
        .then('servicos')
        .into(app);

    return app;
}