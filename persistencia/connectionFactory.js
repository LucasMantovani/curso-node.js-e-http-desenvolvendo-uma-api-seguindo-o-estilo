var mysql = require('mysql');

function createDBConnection() {
    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'payfast'
    });

}

//wrapper das funções para evitar o carregamento automático
module.exports = function () {
    return createDBConnection;
}