var logger  = require('../servicos/logger');

module.exports = function (app) {
    app.get('/pagamentos/pagamento/:id', function (req, res) {
        var id = req.params.id;
        console.log('consultando pagamento: '+ id);
        logger.info('consultando pagamento: '+ id);

        var connection = app.persistencia.connectionFactory();
        var pagamentoDao = new app.persistencia.PagamentoDao(connection);

        pagamentoDao.buscaPorId(id, function (erro, resultado) {
            if (erro) {
                console.log('erro ao consultar no banco: '+ id);
                res.status(500).send(erro);
                return;
            }
            console.log('pagamento encontrado: ' + JSON.stringify(resultado))
            res.json(resultado);
            return;
        })
    });

    app.put('/pagamentos/pagamento/:id', function (req, res) {

        var pagamento = {};
        var id = req.params.id;

        pagamento.id = id;
        pagamento.status = 'CONFIRMADO';

        var connection = app.persistencia.connectionFactory();
        var pagamentoDao = new app.persistencia.PagamentoDao(connection);

        pagamentoDao.atualizar(pagamento, function (erro) {
            if (erro) {
                res.status(500).send(erro);
                return;
            }
            res.send(pagamento);
        })

    });

    app.delete('/pagamentos/pagamento/:id', function (req, res) {

        var pagamento = {};
        var id = req.params.id;

        pagamento.id = id;
        pagamento.status = 'CANCELADO';

        var connection = app.persistencia.connectionFactory();
        var pagamentoDao = new app.persistencia.PagamentoDao(connection);

        pagamentoDao.atualizar(pagamento, function (erro) {
            if (erro) {
                res.status(500).send(erro);
                return;
            }
            res.status(204).send(pagamento);
        })

    });


    app.post('/pagamentos/pagamento', function (req, res) {

        req.assert("pagamento.forma_de_pagamento", "Forma de pagamneto é obrigatorio").notEmpty();
        req.assert("pagamento.valor", "Valor é obrigatorio e deve ser um decimal").notEmpty().isFloat();

        var erros = req.validationErrors();

        if (erros) {
            console.log('Erros de validacao encontrado');
            res.status(400).send(erros);
            return;
        }

        var pagamento = req.body["pagamento"];
        console.log('processando uma requisicao de um novo pagamento');

        pagamento.status = 'CRIADO';
        pagamento.data = new Date;

        var connection = app.persistencia.connectionFactory();
        var pagamentoDao = new app.persistencia.PagamentoDao(connection);

        console.log(pagamento);

        pagamentoDao.salvar(pagamento, function (erro, resultado) {
            if (erro) {
                res.status(400).send(erro)
            }
            else {
                pagamento.id = resultado.insertId;
                console.log('pagamento criado');

                if (pagamento.forma_de_pagamento == 'cartao') {
                    var cartao = req.body["cartao"];
                    console.log(cartao);
                    var clienteCartoes = new app.servicos.clienteCartoes();
                    
                    clienteCartoes.autorizar(cartao, function (exception, request, response, retorno) {
                        
                        if(exception)
                        {
                            console.log(exception);
                            res.status(400).send(exception);
                            return;
                        }
                        
                        console.log(retorno);
                        res.status(201).json(cartao);
                        return;
                    });
                }
                else {
                    res.location('/pagamentos/pagamento/' +
                        pagamento.id);

                    var response = {
                        dados_do_pagamento: pagamento,
                        links: [
                            {
                                href: "http://localhost:3000/pagamentos/pagamento" + pagamento.id,
                                rel: "confirmar",
                                method: "PUT"
                            },
                            {
                                href: "http://localhost:3000/pagamentos/pagamento" + pagamento.id,
                                rel: "cancelar",
                                method: "DELETE"
                            }
                        ]
                    }
                    res.status(201).json(response);
                }
            }
        });
    });
}
