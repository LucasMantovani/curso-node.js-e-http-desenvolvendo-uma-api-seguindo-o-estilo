var cluster = require('cluster');
var os = require('os');

console.log('Executnado thread');


// A primeira coisa é verificar se é o master
// pois ele é o único que pode invocar o fork()
if (cluster.isMaster) {
    console.log('Executnado master')
    //Fork -> Gera uma nova threads, filha da thread principal

    os.cpus().forEach(function () {
        cluster.fork();
    });

    //Obeserva filhos
    cluster.on('listening', function (worker) {
        console.log('cluster conectado ' + worker.process.pid);
    });

    //Caso algum cluster desconect (cai)
    cluster.on('exit', worker => {
        console.log('cluster desconectado ' + worker.process.pid);
        cluster.fork();
    });

   // Se o cluster não for o master, ele deve somente executar o index.js
    // subindo o objeto do express e ficando no ar para receber requisições.
} else {
    console.log('thread slave');
    require('./index.js');
}
